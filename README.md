Latest release of mhwaveedit as published by the author on gna.org in August 2013.

This was taken from the latest snapshot available at archive.org:
https://web.archive.org/web/20170225142259/http://download.gna.org/mhwaveedit/

Kept here as the version on github seems to have a problem to build.

Refer to the original README file for details, but this should build with the usual:

./configure
make

And optionally:

make install
